CREATE DATABASE CadastroHerois;

USE [CadastroHerois]
GO
CREATE TABLE [dbo].[tblHerois](
	[Id] [int] NULL,
	[Nome] [varchar](100) NOT NULL,
	[IdentidadeSecreta] [varchar](100) NOT NULL,
	[Inteligencia] [int] NOT NULL,
	[Forca] [int] NOT NULL,
	[Velocidade] [int] NOT NULL,
	[Durabilidade] [int] NOT NULL,
	[Poder] [int] NOT NULL,
	[Combate] [int] NOT NULL
) ON [PRIMARY]
GO

CREATE PROCEDURE [dbo].[sp_cadastra_heroi]
(
	@in_id int, 			
	@in_nome VARCHAR(100) = NULL,
	@in_identidadeSecreta VARCHAR(100)= NULL,
	@in_inteligencia int = NULL,
	@in_forca int= NULL,
	@in_velocidade int = NULL,	
	@in_durabilidade INT = NULL,	
	@in_poder INT = NULL,
	@in_combate INT = NULL
)	
AS
BEGIN
	if(Exists(Select id from tblHerois where id = @in_id))
	begin
		UPDATE tblHerois 
			set Id = @in_id 
			,Nome = @in_combate
			,IdentidadeSecreta = @in_identidadeSecreta
			,Inteligencia = @in_inteligencia
			,Forca = @in_forca
			,Velocidade = @in_velocidade
			,Durabilidade = @in_durabilidade
			,Poder= @in_combate
			,Combate = @in_combate
			Where id = @in_id
	end
	else
	begin
		INSERT INTO [dbo].[tblHerois]
           ([Id]
           ,[Nome]
           ,[IdentidadeSecreta]
           ,[Inteligencia]
           ,[Forca]
           ,[Velocidade]
           ,[Durabilidade]
           ,[Poder]
           ,[Combate])
     VALUES
           (@in_id
           ,@in_nome
           ,@in_identidadeSecreta
           ,@in_inteligencia 
           ,@in_forca
           ,@in_velocidade 
           ,@in_durabilidade 
           ,@in_poder
           ,@in_combate)
	end
END
GO

CREATE PROCEDURE [dbo].[sp_retorna_heroi]
(
	@in_id int = null, 			
	@in_nome VARCHAR(100) = NULL
)	
AS
BEGIN
	if(@in_id IS NOT NULL)
	begin
		Select Id
			,Nome
			,IdentidadeSecreta
			,Inteligencia
			,Forca
			,Velocidade
			,Durabilidade
			,Poder
			,Combate
		From tblHerois
		Where Id = @in_id
	end
	if(@in_id IS NOT NULL)
	begin
		Select Id
			,Nome
			,IdentidadeSecreta
			,Inteligencia
			,Forca
			,Velocidade
			,Durabilidade
			,Poder
			,Combate
		From tblHerois
		Where Nome like '%'+@in_nome+'%' 
			OR IdentidadeSecreta like '%'+@in_nome+'%'
	end
END
GO