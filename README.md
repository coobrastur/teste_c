# Teste desenvolvedor c# Coobrastur #

Bem vindo ao teste de desenvolvedor c# da Coobrastur, siga as instruções descritas abaixo e bom teste.

### Deverá ser utilizado a api publica do site: [Super Herois API](https://superheroapi.com/) ###
### Deverá ser criado um fork do repositório atual e após terminado enviar uma pull request pra esse repositório. ###
* Utilizar o versionamento GIT para desenvolvimento.

### Deverá ser criado um projeto webapi em .net Core 3.1 ou superior, utilizando as boas praticas e clean code no desenvolvimento. ###
* Utilizar o arquivo scriptbancoTeste.sql para criar a database, tabelas e Procedures.
* Configurar o swagger 
* Criar endpoints:
	* Para listar todos os Heóis que estaão na base utilizando a procedure "sp_retorna_heroi", ordernar por ordem alfabetica do Nome
	* Para retornar heroi por Nome ou identidade secreta.
	* Para Criar ou editar um herói, validar se o mesmo ja esta na base.
	* Para Retornar 30 heróis em ordem alfabetica no de identidade secreta, com os mesmos campos da tabela "Herois" do banco, utilizar a api Super Herois API para obter os mesmo.

Boa Prova.